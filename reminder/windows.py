# coding=utf-8
from win10toast import ToastNotifier
from bs4 import BeautifulSoup
import sys, requests, json, re

requests.adapters.DEFAULT_RETRIES = 5
config_path = "config.json"
icon_path = "image/windows.ico"
default_seconds = 2
notitifcation_seconds = 10
default_title = "Comic Reminder"
default_content = "Comic Reminder!"

def show_start_notification():
    global toaster
    toaster = ToastNotifier()
    toaster.show_toast(default_title, default_content, icon_path=icon_path, duration=default_seconds)
    
def show_update_notification(name, my_num, max_num):
    title = default_title
    content = "{}目前看至第{}話，已更新至第{}話!".format(name, my_num, max_num)
    toaster.show_toast(default_title, content, icon_path=icon_path, duration=notitifcation_seconds)

def read_config():
    global dm5_config, manhua_config
    config = json.loads(open(config_path).read())
    dm5_config = config["dm5"]
    manhua_config = config["manhua"]

def get_links(url):
    response = requests.get(url)
    response.encoding= "utf-8"
    soup = BeautifulSoup(response.text.encode("utf-8"), "html.parser")
    links = soup.findAll("a")
    links = filter(lambda link: (link.parent.name == "li") ,links)
    return links

def check_comic_notification(name, url, my_num, pattern):
    try:
        links = get_links(url)
        nums = []
        for link in links:
            text = str(link.text.encode("utf-8"))
            regex_match = re.match(pattern, text)
            if regex_match != None:
                regex_group = regex_match.groups()
                if len(regex_group) >= 1:
                    num = int(regex_group[0])
                    nums.append(num)
        if len(nums) == 0:
            sys.stderr.write("Something Wrong!")
            return
        max_num = max(nums)
        if max_num > my_num:
            show_update_notification(name, my_num, max_num)
    except Exception,err:
        print err
        sys.stderr.write("Connection Failed\n")

def generate_pattern(display):
    display_prefix = display["prefix"].encode("utf-8")
    display_suffix = display["suffix"].encode("utf-8")
    pattern = "{}(\d+){}".format(display_prefix, display_suffix)
    return pattern
        
def check_dm5():
    comics = dm5_config["comic"]
    url_base = dm5_config["url_base"]
    pattern = generate_pattern(dm5_config["display"])
    for comic in comics:
        name = comic["name"]
        my_num = comic["number"]
        url = url_base+comic["url_suffix"]
        check_comic_notification(name, url, my_num, pattern)

def check_manhua():
    comics = manhua_config["comic"]
    pattern = generate_pattern(manhua_config["display"])
    for comic in comics:
        name = comic["name"].encode("utf-8")
        my_num = comic["number"]
        url = comic["url"]
        check_comic_notification(name, url, my_num, pattern)
    
def start_reminder():
    while True:
        check_dm5()
        check_manhua()
        break
    
def main():
    show_start_notification()
    read_config()
    start_reminder()    
    
if __name__ == '__main__':
    show_start_notification()