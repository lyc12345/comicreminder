#!/usr/bin/python3
import io
import os
import notify2
import requests
import time
import json
from bs4 import BeautifulSoup


notify2.init('comic')
image = "file://"+os.path.abspath(os.path.curdir)+"/cool.jpg"
a = notify2.Notification("Comic Watcher Start!!","comics",image)
a.show()
time.sleep(100)

while True:
    comics = json.loads(io.open("/home/lyc12345/comic/comic.json",encoding='utf8').read())
    for comic in comics:
        name = comic['name']
        url = comic['url']
        number = comic['number']
        diaplay = None
        if 'display' in comic:
            display = comic['display']
        try:
            response = requests.get(url)
            response.encoding='utf-8'
            soup = BeautifulSoup(response.text.encode("utf-8"))
            if "dmeden.net" in url:
                linklist = soup.findAll('a',href=True)
                for link in linklist:
                    string =str(link.text)
                    if len(string)<len(name):
                        continue
                    if string.find(name)>=0 and string.find(str(number).zfill(2))>=0:
                        image = "file://"+os.path.abspath(os.path.curdir)+"/cool.jpg"
                        n = notify2.Notification("Comic update!!",name+"已更新",image)
                        n.show()
                        break
            elif "http://www.dm5.com/" in url or "http://comic.sfacg.com/" in url:
                linklist = soup.findAll('a')
                for link in linklist:
                    string =str(link.text)
                    if link.parent.name == 'li':
                        if display != None and display in string and string.find(str(number))>=0:
                            image = "file://"+os.path.abspath(os.path.curdir)+"/cool.jpg"
                            n = notify2.Notification("Comic update!!",name+"已更新",image)
                            n.show()
                            break
                        elif display == None and string.find(str(number).zfill(3))>=0:
                            image = "file://"+os.path.abspath(os.path.curdir)+"/cool.jpg"
                            n = notify2.Notification("Comic update!!",name+"已更新",image)
                            n.show()
                            break
        except:
            print("connect failed")
        time.sleep(5)
    time.sleep(3600)
